#!/usr/bin/env bash

set -ueo pipefail

git clone https://github.com/KyleGospo/gamescope-session.git /tmp/gamescope-session

cd /tmp/gamescope-session

cp -rf usr/* /usr

[ -f /usr/share/gamescope-session-plus/gamescope-session-plus ]
