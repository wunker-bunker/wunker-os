#!/usr/bin/env bash

set -euo pipefail

cd /usr/bin/

curl -L "https://github.com/vadimcn/vscode-lldb/releases/download/v1.7.0/codelldb-x86_64-linux.vsix" -o "codelldb-x86_64-linux.zip"

unzip "codelldb-x86_64-linux.zip" "extension/adapter/*" "extension/lldb/*" > /dev/null

rm -f "codelldb-x86_64-linux.zip"

mv extension/ codelldb_adapter

ln -s $(pwd)/codelldb_adapter/adapter/codelldb /usr/bin/codelldb

codelldb -h > /dev/null