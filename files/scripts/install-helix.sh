#!/usr/bin/env bash

set -euo pipefail

apt-get update && apt-get install -y git

mkdir -p /src/
cd /src/

git clone https://github.com/helix-editor/helix.git
cd helix

rustup toolchain install 1.76.0-x86_64-unknown-linux-gnu

export HELIX_DEFAULT_RUNTIME=/usr/lib/helix/runtime
cargo build --profile opt --locked

mkdir -p /out/
mv target/opt/hx /out/hx
mv runtime /out/
