#!/usr/bin/env nu

http get https://vault.bitwarden.com/download/?app=cli&platform=linux | save /tmp/bitwarden.zip

^unzip /tmp/bitwarden.zip -d /usr/bin/
rm /tmp/bitwarden.zip
