#!/usr/bin/env nu

let download_url = (
  (http get https://api.github.com/repos/zellij-org/zellij/releases | enumerate | first).item.assets
  | find -c [name] "x86_64-unknown-linux-musl.tar.gz"
).browser_download_url.0

http get -r $download_url | save /tmp/zellij.tar.gz
tar -xvf /tmp/zellij.tar.gz
mv zellij /usr/bin/
rm /tmp/zellij.tar.gz
zellij -V
