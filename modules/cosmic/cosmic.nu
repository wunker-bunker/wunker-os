#!/usr/bin/env nu

def main [config: string]: nothing -> nothing {
  let config = $config | from json | default false debug
  print $config
  let debug = $config.debug | if $in { 1 } else { 0 }
  let bin_dir = $config.debug | if $in { "debug" } else { "release" }

  try {
    echo "deb http://deb.debian.org/debian bookworm-backports main contrib non-free-firmware" | save --append /etc/apt/sources.list
    apt-get update
    apt-get install -y git libudev-dev libseat-dev libxkbcommon-dev libinput-dev libgbm-dev libdisplay-info-dev
  } catch {
    print 'Failed to install dependencies'
    exit 1
  }
  mkdir /tmp/src/
  cd /tmp/src/

  try {
    git clone $'https://github.com/pop-os/($config.component).git'
    cd $config.component

    if ('ref' in $config) {
      git checkout $config.ref
    }
  } catch {
    print $'Failed to checkout cosmic component ($config.component)'
    exit 1
  }

  try {
    make vendor
    make all $'DEBUG=($debug)' VENDOR=1
  } catch {
    print $'Failed to build component ($config.component)'
    exit 1
  }

  mkdir /out/
  mv $'target/($bin_dir)/($config.component)' /out/
}
