#!/usr/bin/env bash

set -euo pipefail

VERSION=$(echo $1 | jq -r .version)

echo "Installing R2modman Plus v${VERSION}"

# Download the latest version of the r2modman package
curl -L0 https://github.com/ebkr/r2modmanPlus/releases/download/v${VERSION}/r2modman-${VERSION}.x86_64.rpm -o r2modman-${VERSION}.x86_64.rpm

mkdir -p "/var/opt"
ln -s "/var/opt"  "/opt"
mkdir -p "/usr/lib/opt/r2modman"
ln -s "../../usr/lib/opt/r2modman" "/var/opt/r2modman"
# Install the downloaded package
rpm-ostree install r2modman-${VERSION}.x86_64.rpm

