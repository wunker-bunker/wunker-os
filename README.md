# wunker-os

[![pipeline status](https://gitlab.com/wunker-bunker/wunker-os/badges/main/pipeline.svg)](https://gitlab.com/wunker-bunker/wunker-os/-/commits/main)

For more info, check out the [blue-build homepage](https://blue-build.org/).

![bluebuild](bluebuild.gif)

## Installation

To rebase an existing Silverblue/Kinoite installation to the latest build:

```bash
sudo rpm-ostree rebase ostree-image-signed:docker://registry.gitlab.com/wunker-bunker/wunker-os/jp-desktop:39
```

## Verification

These images are signed with sisgstore's [cosign](https://docs.sigstore.dev/cosign/overview/). You can verify the signature by running the following command:

```bash
cosign verify --certificate-identity "https://gitlab.com/wunker-bunker/wunker-os//.gitlab-ci.yml@refs/heads/main" --certificate-oidc-issuer "https://gitlab.com" registry.gitlab.com/wunker-bunker/wunker-os/<VARIANT>:<TAG>
```

Where `VARIANT` is the image from the recipe you choose, and `TAG` is the version of the image.
